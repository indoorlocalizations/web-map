import {Leguide} from './guide/leguide'
import examplemap from './examplemap'
import Leaflet from "leaflet";
import {LevelLatLng} from "./guide/LevelPath";

document.addEventListener('DOMContentLoaded', () => {
    const leGuideWrapper = typeof LeguideApp !== 'undefined';
    console.log("Leguide wrapper:" + (leGuideWrapper ? "Y" : "N"));

    let map = new Leguide('map');

    const appEvent = function (version, type, data) {
        if (type === "position") {
            map.notifyPosition(Leaflet.latLng(data.latitude, data.longitude), data.level, data.accuracy, data.compass);
        } else if (type === "loadmap") {
            map.loadMap(JSON.parse(data));
        } else if (type === "beacons") {
            map.showBeacons(data.map((it) => {
                return {
                    coords: Leaflet.latLng(it.latitude, it.longitude),
                    radius: it.radius,
                    level: it.level
                }
            }))
        } else if (type === "showpoint") {
            map.showMarker(Leaflet.latLng(data.latitude, data.longitude), data.level, data.title)
        } else if (type === "showpath") {
            let path = data.path.map((r) => {
                return new LevelLatLng(Leaflet.latLng(r.latitude, r.longitude), r.level)
            });
            console.log(path);
            map.showPath(path);
        }
    };
    window.appEvent = appEvent;

    if (leGuideWrapper) {
        map.loadMap(JSON.parse(LeguideApp.getMapManifestObject()));
    } else {
        map.loadMap(examplemap);
    }
});
