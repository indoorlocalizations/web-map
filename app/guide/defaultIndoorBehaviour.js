// @flow

import Leaflet from 'leaflet'

const behaviour = {
    pointToLayer: function(geoJsonPoint: any, latlng: Leaflet.LatLng) {
        return;
    },

    parseLevel(input: string) {
        let regexp = /^((-?[1-9][0-9]*)|[0])(-((-?[1-9][0-9]*)|[0]))?$/
        const flatMap = (arr, f) => [].concat.apply([], arr.map(f))
        return flatMap(input.split(";"), function (it) {
            let result = regexp.exec(it);
            if (result) {
                let low = parseInt(result[1]);
                if (result[3] != null) {
                    let high = parseInt(result[4]);
                    return Array.from(new Array(high + 1 - low), (x,i) => i + low);
                } else {
                    return [low]

                }
            }
        });
    },

    getLevel: function (feature: any) {
        var output = [];
        if (typeof feature.properties === "object" && 'level' in feature.properties) {
            output = output.concat(this.parseLevel(feature.properties["level"]));
        }
        if (typeof feature.properties === "object" && 'repeat_on' in feature.properties) {
            output = output.concat(this.parseLevel(feature.properties["repeat_on"]));
        }
        return output;
    },
    pointToLayer: function (feature: any, coords: Leaflet.LatLng) {
        if (feature.properties['highway'] === 'elevator') {
            return Leaflet.marker(coords, {
                icon: Leaflet.icon({
                    iconUrl: 'images/elevator.png',
                    iconSize: [20, 20]
                })
            });
        }

        if (feature.properties['door'] === 'yes') {
            var icon = 'images/door.png';
            if (['main', 'exit', 'yes'].includes(feature.properties['entrance'])) {
                icon = 'images/exit.png';
            }

            return Leaflet.marker(coords, {
                icon: Leaflet.icon({
                    iconUrl: icon,
                    iconSize: [20, 20]
                })
            });
        }
    },
    markerForFeature: function (feature: any, layer: Leaflet.Feature) {
        if (feature.geometry.type === "Polygon") {

            const name = "name" in feature.properties ? feature.properties.name : "";
            var icon = Leaflet.divIcon({
                className: 'label',
                html: name,
            });
            if (feature.properties['amenity'] === 'toilets') {
                icon = Leaflet.icon({
                    iconUrl: 'images/toilet.png',
                    iconSize: [20, 20]
                });
            } else if (feature.properties['stairs'] === 'yes') {
                icon = Leaflet.icon({
                    iconUrl: 'images/stairs.png',
                    iconSize: [20, 20]
                });
            }
            return Leaflet.marker(layer.getBounds().getCenter(), {
                icon: icon
            });
        }
    },
    onEachFeature: function (feature: any, layer: Leaflet.Feature) {
        if ("name" in feature.properties) { // description?
            layer.bindPopup(feature.properties.name);
        }
    },
    style: function (feature: any) {
        let fill = 'white';

        if (feature.properties['stairs'] === 'yes') {
            fill = '#d5f4e6';
        }

        if (feature.properties['amenity'] === 'toilets') {
            fill = '#169EC6';
        }

        return {
            fillColor: fill,
            weight: 1,
            color: '#666',
            fillOpacity: 1
        };
    }
};

export default behaviour;