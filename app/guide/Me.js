import L from 'leaflet';
import rotate from 'leaflet-plugins/layer/Marker.Rotate';

const LOCK_STATE_EVENT = 'changelock';
const DEFAULT_LOCK_STATE = false;

type LockEvent = {
    locked: boolean
};

const MeLockControl = L.Control.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    options: {
        position: 'bottomright',
        lockStateChanged: (locked) => {}
    },

    initialize(options) {
        L.setOptions(this, options);

        this._map = null;
        this._button = null;
        this._stateLocked = DEFAULT_LOCK_STATE;
        this._ignoreLockChange = false;
    },

    onAdd(map) {
        let div = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
        div.style.font = "30px 'Lucida Console',Monaco,monospace";

        let button = L.DomUtil.create('a', 'leaflet-button-part', div);
        button.style.fontWeight = "bold";
        button.appendChild(button.ownerDocument.createTextNode("☉")); // 🔘
        button.onclick = () => this.toggleLock();
        this._button = button;
        this._stateLocked = DEFAULT_LOCK_STATE;
        this._changeButtonByState(DEFAULT_LOCK_STATE);

        return div;
    },

    setLockState(locked: boolean) {
        if (this._stateLocked === locked || this._ignoreLockChange) {
            return;
        }

        this._ignoreLockChange = true;
        this._changeButtonByState(locked);
        this._stateLocked = locked;
        this.fireEvent(LOCK_STATE_EVENT, {
            locked: locked
        });

        // timeout is here because of focusing on person may produce events which makes unlocks (e.g. zoom)
        setTimeout(() => this._ignoreLockChange = false, 1000);
    },

    toggleLock() {
        this.setLockState(!this.isLocked());
    },

    isLocked() {
        return this._stateLocked;
    },

    _changeButtonByState(state: boolean) {
        if (state === true) {
            this._button.style.color = "red";
        } else {
            this._button.style.color = "black";
        }
    }

});

const MeMarker = L.Layer.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    options: {},

    initialize(options) {
        L.setOptions(this, options);
        this._map = null;
        this._personLevel = NaN;
        this._viewLevel = 0;
        this._knowSomePosition = false;
        //this._marker = L.circleMarker([0, 0], {color: "#d63031", radius: 1});
        this._marker = L.marker([0, 0], {icon: L.icon({iconUrl: 'images/point-orientation.png', zIndexOffset: 1000, iconSize:[50, 50], iconAnchor: [25, 25]})});
        this._accuracy = L.circle([0, 0], {color: "#74b9ff", zIndexOffset: 1000, opacity: 0.5});
        this._accuracyValue = 1;
        this._compass = NaN;

        this._locked = DEFAULT_LOCK_STATE;
    },

    getPersonLevel() {
        return this._personLevel;
    },

    onAdd(map) {
        this._map = map;
    },

    registerToControl(control: MeLockControl) {
        control.addEventListener(LOCK_STATE_EVENT, this._changeLockEvent, this);
    },

    changePosition(coords: L.LatLng, level: number, accuracy: number = 1, compass = NaN) {
        this._marker.setLatLng(coords);
        this._accuracy.setLatLng(coords);
        this._accuracy.setRadius(accuracy);

        this._accuracyValue = accuracy;
        this._compass = compass;
        if (!isNaN(compass) || compass == null) {
            this._marker.setIconAngle(compass);
        }

        this._personLevel = level;
        this._knowSomePosition = true;

        if (this._locked) {
            this._viewLevel = level;
        }

        if (this._map !== null && this._locked) {
            this._map.setView(coords, 22);
        }
        this._invalidateIcon();
    },

    changeViewLevel(level) {
        this._viewLevel = level;
        this._invalidateIcon();
    },

    _invalidateIcon() {
        this._accuracy.removeFrom(this._map);
        this._marker.removeFrom(this._map);
        if (this._viewLevel === this._personLevel) {
            this._accuracy.addTo(this._map);
            this._marker.addTo(this._map);
        }
    },

    _changeLockEvent: function (e: LockEvent) {
        this._locked = e.locked;
        if (this._knowSomePosition) {
            this.changePosition(this._marker.getLatLng(), this._personLevel, this._accuracyValue, this._compass);
        }
    }

});

export {MeLockControl, MeMarker, LOCK_STATE_EVENT};
