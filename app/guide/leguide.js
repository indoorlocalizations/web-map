// @flow

import Leaflet from 'leaflet'
import {LevelControl} from '../libs/LevelControl'
import {Indoor} from '../libs/IndoorLayer'
import {LOCK_STATE_EVENT, MeLockControl, MeMarker} from './Me'
import defaultIndoorBehaviour from './defaultIndoorBehaviour';
import {LevelMarker} from "./LevelMarker";
import {LevelPath, LevelLatLng} from "./LevelPath";
import {BeaconsMarker} from "./Beacons";

export class Leguide {

    map: Leaflet.Map;
    indoorLayer: Indoor;
    levelControl: LevelControl;
    meLockControl: MeLockControl;
    levelMarker: LevelMarker;
    levelPath: LevelPath;
    beacons: BeaconsMarker;

    person: MeMarker;

    constructor(elementId: string) {

        this.map = new Leaflet.Map(elementId, {
            zoom: 20,
            minZoom: 20,
            attributionControl: false,
        });

        // openstreetmap: //{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
        Leaflet.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic29kYWUiLCJhIjoiY2oxN3Nvam53MDA1dzMybjVpcmE3ZXQ1bCJ9.skhez8w2LUs6nuDJ5PRdHA', {
            maxZoom: 22,
            id: 'mapbox.streets'
        }).addTo(this.map);

        this.meLockControl = new MeLockControl();
        this.meLockControl.addTo(this.map);

        this.person = new MeMarker();
        this.person.addTo(this.map);
        this.person.registerToControl(this.meLockControl);

        this.levelMarker = new LevelMarker();
        this.levelMarker.addTo(this.map);

        this.levelPath = new LevelPath();
        this.levelPath.addTo(this.map);

        this.beacons = new BeaconsMarker();
        this.beacons.addTo(this.map);
    }

    notifyPosition(position: Leaflet.LatLng, level: number, accuracy: number, compass: number) {
        this.person.changePosition(position, level, accuracy, compass);
        if (this.indoorLayer !== null && this.meLockControl.isLocked()) {
            this.changeLevel(level);
        }
    }

    showMarker(position: Leaflet.LatLng, level: number, title: String) {
        this.map.setView(position, 22);
        this.meLockControl.setLockState(false);
        this.changeLevel(level);
        this.levelMarker.changePosition(position, level, title);
    }

    showPath(path: LevelLatLng[]) {
        console.log(path);
        if (path.length === 0) return;

        this.meLockControl.setLockState(false);
        this.map.setView(path[0].latLng, 22);
        this.changeLevel(path[0].level);
        this.levelPath.showPath(path);
    }

    loadMap(data: any): void {
        let geo = typeof data.geoJson === 'object' ? data.geoJson : JSON.parse(data.geoJson);
        this._initIndoor(geo);
        this.map.setView(this._toCoords(data.center), data.defaultZoom);
        this.map.setMaxBounds([this._toCoords(data.boundaries[0]), this._toCoords(data.boundaries[1])]);
        this.map.setMinZoom(data.allowedZoom);
        this.changeLevel(data.defaultLevel);
        this.meLockControl.setLockState(true);
    }

    changeLevel(level: number) {
        if (isNaN(level)) {
            return;
        }
        this.indoorLayer.setLevel(level);
        this.levelControl.setLevel(level);
        this.levelMarker.changeViewLevel(level);
        this.levelPath.changeViewLevel(level);
        this.person.changeViewLevel(level);
        this.beacons.changeViewLevel(level);
    }

    showBeacons(beacons: any) {
        this.beacons.changePosition(beacons);
    }

    _initIndoor(geoJson: any): void {
        if (typeof this.levelControl !== 'undefined') {
            this.map.removeLayer(this.levelControl);
        }

        if (typeof this.levelControl !== 'undefined') {
            this.levelControl.remove();
        }

        this.indoorLayer = new Indoor(geoJson, defaultIndoorBehaviour);

        let level = this.indoorLayer.getLevels()[0];
        this.levelControl = new LevelControl({
            level: level,
            levels: this.indoorLayer.getLevels()
        });

        this.meLockControl.addEventListener(LOCK_STATE_EVENT, (e) => this.changeLevel(this.person.getPersonLevel()));
        this.levelControl.addEventListener("levelchange", (e) => this.changeLevel(e.level), this.indoorLayer);

        // user interaction
        this.levelControl.addEventListener("levelchange", () => this.meLockControl.setLockState(false), this.indoorLayer);
        this.map.addEventListener("drag", () => this.meLockControl.setLockState(false), this.indoorLayer);
        this.map.addEventListener("zoomstart", () => this.meLockControl.setLockState(false), this.indoorLayer);

        this.indoorLayer.addTo(this.map);
        this.levelControl.addTo(this.map);
    }

    _toCoords(obj: any) {
        console.log(JSON.stringify(obj));
            return Leaflet.latLng(obj.latitude, obj.longitude, obj.altitude);
    }

}
