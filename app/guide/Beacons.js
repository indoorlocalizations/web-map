import L from 'leaflet';
import rotate from 'leaflet-plugins/layer/Marker.Rotate';

const BeaconsMarker = L.Layer.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    options: {},

    initialize(options) {
        L.setOptions(this, options);
        this._map = null;
        this._personLevel = NaN;
        this._viewLevel = 0;
        this._placed = [];
        this._beacons = []
    },

    getPersonLevel() {
        return this._personLevel;
    },

    onAdd(map) {
        this._map = map;
    },

    registerToControl(control: MeLockControl) {
        control.addEventListener(LOCK_STATE_EVENT, this._changeLockEvent, this);
    },

    changePosition(beacons: any) {
        this._beacons = beacons;
        this._invalidate();
    },

    changeViewLevel(level) {
        this._viewLevel = level;
        this._invalidate();
    },

    _invalidate() {
        this._placed.forEach((obj) => {
            obj.removeFrom(this._map);
        });

        this._placed = [];

        this._beacons
            .filter((obj) => obj.level == this._viewLevel )
            .forEach((obj) => {
                let element = L.circle(obj.coords, {color: "#FF3535", zIndexOffset: 1000, opacity: 0.5, radius: obj.radius})
                this._placed.push(element);
                element.addTo(this._map);
            });
    },

    _changeLockEvent: function (e: LockEvent) {
        this._locked = e.locked;
        if (this._knowSomePosition) {
            this.changePosition(this._marker.getLatLng(), this._personLevel, this._accuracyValue, this._compass);
        }
    }

});

export {BeaconsMarker};
