import L from 'leaflet';
import { AntPath, antPath } from 'leaflet-ant-path';

const PathCancelButton = L.Control.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    callback: null,

    cancelPath() {
        if (this.callback != null) {
            this.callback();
        }
    },

    onAdd(map) {
        this._map = map;

        let div = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
        div.style.font = "1em 'Lucida Console',Monaco,monospace";

        let button = L.DomUtil.create('a', 'leaflet-button-part', div);
        button.style.fontWeight = "bold";
        button.appendChild(button.ownerDocument.createTextNode("X"));
        button.onclick = () => this.cancelPath();
        this._cancelButton = button;
        this._cancelButtonDiv = div;

        return this._cancelButtonDiv;
    },

});

const LevelPath = L.Layer.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    initialize(options) {
        L.setOptions(this, options);
        this._map = null;
        this._line = null;
        this._finalMarker = null;
        this._path = null;
        this._viewLevel = 0;
        this._control = new PathCancelButton();
        this._control.callback = () => {
            this._path = null;
            this._invalidatePath();
        }
    },

    onAdd(map) {
        this._map = map;
    },

    showPath(path: LevelLatLng[]) {
        this._path = path;
        this._invalidatePath();
    },

    changeViewLevel(level) {
        this._viewLevel = level;
        this._invalidatePath();
    },

    _invalidatePath() {
        if (this._line != null) {
            this._line.removeFrom(this._map);
        }
        if (this._finalMarker) {
            this._finalMarker.removeFrom(this._map);
        }

        if (this._path == null || this._path.length === 0) {
            this._control.remove(this._map);
            return;
        }

        let latlngs = [];
        for (let i = 0; i < this._path.length - 1; i++) {
            let currentElement = this._path[i];
            let nextElement = this._path[i + 1];
            if (currentElement.level === this._viewLevel || nextElement.level === this._viewLevel) {
                latlngs.push(currentElement.latLng);
            }
            if (i === this._path.length - 2 && nextElement.level === this._viewLevel) {
                latlngs.push(nextElement.latLng);
            }
        }

        this._line = L.polyline(latlngs, {color: 'red', dashArray: '1, 5'}); //antPath(latlngs);
        this._line.addTo(this._map);

        let finalPoint = this._path[this._path.length - 1];
        if (finalPoint.level === this._viewLevel) {
            this._finalMarker = L.circleMarker(finalPoint.latLng, {color: 'red'});
            this._finalMarker.addTo(this._map);
        }

        this._control.addTo(this._map);
    },

});

class LevelLatLng {

    latLng: L.LatLng;
    level: Double;

    constructor(latLng: L.LatLng, level: Double) {
        this.latLng = latLng;
        this.level = level;
    }

}

export {LevelPath, LevelLatLng};
