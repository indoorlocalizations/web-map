import L from 'leaflet';

const LevelMarker = L.Layer.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    initialize(options) {
        L.setOptions(this, options);
        this._map = null;
        this._marker = null;
        this._markerLevel = 0;
        this._viewLevel = 0;
    },

    onAdd(map) {
        this._map = map;
    },

    changePosition(coords: L.LatLng, level: number, title: String) {
        console.log("c", coords, level);

        if (this._marker != null) {
            this._marker.removeFrom(this._map);
        }
        this._markerLevel = level;

        this._marker = L.marker(coords, {
            title: title,
        });
        this._marker.bindPopup(title).openPopup();

        this._marker.addTo(this._map);
        this._invalidateIcon();
    },

    changeViewLevel(level) {
        this._viewLevel = level;
        this._invalidateIcon();
    },

    _invalidateIcon() {
        if (this._marker != null) {
            if (this._viewLevel === this._markerLevel) {
                this._marker.addTo(this._map);
            } else {
                this._marker.removeFrom(this._map);
            }
        }
    },

});

export {LevelMarker};
