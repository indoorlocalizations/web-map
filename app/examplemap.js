import geoJSON from './vsb-indoor.json';

let lat = 49.83143407800;
let long = 18.16117170859;

let toCoords = (arr) => {
    return {
        latitude: arr[0],
        longitude: arr[1],
        altitude: arr.length === 3 ? arr[2] : 0,
    }
};

const map = {
    center: toCoords([lat, long]),
    boundaries: [
        toCoords([lat + 0.001, long - 0.001]),
        toCoords([lat - 0.001, long + 0.001])
    ],
    defaultZoom: 19,
    defaultLevel: 4,
    geoJson: geoJSON
};

export default map;
