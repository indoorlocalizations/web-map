import L from 'leaflet';

const LevelControl = L.Control.extend({
    includes: L.Evented.prototype || L.Mixin.Events,

    options: {
        position: 'bottomright',
        levels: [],

        // used to get a unique integer for each level to be used to order them
        parseLevel: function (level: String) {
            return parseFloat(level, 10);
        }
    },

    initialize: function (options) {
        L.setOptions(this, options);

        this._map = null;
        this._buttons = {};
        this._listeners = [];
        this._level = options.level;
    },
    onAdd: function (map) {
        const div = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
        div.style.font = "18px 'Lucida Console',Monaco,monospace";

        const buttons = this._buttons;
        const self = this;

        const levels = [];

        this.options.levels.forEach((level) => {
            levels.push({
                num: self.options.parseLevel(level),
                label: level
            });
        });

        levels.sort((a, b) => {
            return a.num - b.num;
        });

        for (let i = levels.length - 1; i >= 0; i--) {
            const level = levels[i].num;

            const levelBtn = L.DomUtil.create('a', 'leaflet-button-part', div);
            levelBtn.appendChild(levelBtn.ownerDocument.createTextNode(levels[i].label));
            levelBtn.onclick = function () {
                self._clickLevelButton(level);
            };
            buttons[level] = levelBtn;
        }

        return div;
    },
    setLevel(level: Number) {
        let oldLevel = this._level;
        this._level = level;

        if (this._map !== null) {
            if (typeof oldLevel !== "undefined" && oldLevel in this._buttons)
                this._buttons[oldLevel].style.backgroundColor = "#FFFFFF";
            this._buttons[level].style.backgroundColor = "#b0b0b0";
        }
    },

    _clickLevelButton: function (level: Number) {
        if (level === this._level)
            return;

        this.fireEvent("levelchange", {
            level: level
        });
    },
    getLevel: function () {
        return this._level;
    }
});

export {LevelControl};